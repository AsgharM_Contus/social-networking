//
//  DetailedViewController.swift
//  Social Network
//
//  Created by user on 29/01/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class DetailedViewController: UIViewController {
    
    @IBOutlet var name: UILabel!
    @IBOutlet var email: UILabel!
    @IBOutlet var bio: UILabel!
    var p_mail = ""
    var p_name = ""
    var p_bio = ""
    var mail_array = ["pasupathy@contus.in", "santhosh.s@contus.in",
                      "kalandar.b@contus.in"]
    override func viewDidLoad() {
        super.viewDidLoad()
        p_mail = mail_array[myGlobalVariables.iRow]
        //p_name = UserDefaults.standard.string(forKey: "name")!
        p_bio = UserDefaults.standard.string(forKey: "bio")!
        name.text = p_name
        email.text = p_mail
        bio.text = p_bio
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
