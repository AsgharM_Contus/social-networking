//
//  secondViewController.swift
//  Social Network
//
//  Created by user on 23/01/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class secondViewController: UIViewController ,UITableViewDataSource, UITableViewDelegate{
    @IBOutlet var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emp_id.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        cell.textLabel?.text = emp_id[indexPath.row]
        cell.detailTextLabel?.text = emp_name[indexPath.row]
        return cell
        
    }
    
    
    var emp_id = ["CN - 001", "CN - 002", "CN - 003"]
    var emp_name = ["Asghar", "Jagen", "Bhuvana"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
