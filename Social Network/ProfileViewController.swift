//
//  ProfileViewController.swift
//  Social Network
//
//  Created by user on 25/01/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet var p_name: UILabel!
    @IBOutlet var p_mail: UILabel!
    @IBOutlet var p_phone:UILabel!
    @IBOutlet var p_image:UIImageView!
    var mail = ""
    var name = ""
    var phone = ""
    var image = ""
   

    override func viewDidLoad() {
        super.viewDidLoad()
        mail = UserDefaults.standard.string(forKey: "mail")!
        name = UserDefaults.standard.string(forKey: "name")!
        phone = UserDefaults.standard.string(forKey: "phone")!
        image = UserDefaults.standard.string(forKey: "image")!
        
        p_name.text = name
        p_mail.text = mail
        p_phone.text = phone
        p_image.image = UIImage(named: image)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
