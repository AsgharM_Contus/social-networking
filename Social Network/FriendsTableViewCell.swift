//
//  FriendsTableViewCell.swift
//  Social Network
//
//  Created by user on 25/01/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {
    
    @IBOutlet var f_image: UIImageView!
    
    @IBOutlet var f_name: UILabel!
    @IBOutlet var f_place: UILabel!
    @IBOutlet var f_email: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
