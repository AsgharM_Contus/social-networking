//
//  ViewController.swift
//  Social Network
//
//  Created by user on 22/01/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var username_input: UITextField!
    @IBOutlet var password_input: UITextField!
    @IBOutlet var table: UIButton!
    
    @IBAction func tableAction(_ sender: Any) {
        self.performSegue(withIdentifier: "gotosec", sender: self)
        
    }
    var t_mail = ""
    var username_array = ["asghar@contus.in",
                          "bhuvi@contus.in"]
    
    var userdetails_dict = ["asghar@contus.in": ["Asghar Khaled M", "asgharkhaled@contus.in", "+91 9597817276", "asghar"],
                            "bhuvi@contus.in": ["Bhuvaneswari L", "bhuvaneswari.l@contus.in", "+91 9500650707", "bhuvi"]]
  
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPassword(testStr:String) -> Bool{
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()-_=+{}|?>.<,:;~`’]{8,}$"
        print("Password type wrong")
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: testStr)
    }
    
    func showAlert(name: String){
        let alertcontroller = UIAlertController(title: "Welcome to Social Networking", message: name, preferredStyle: UIAlertControllerStyle.alert)
        alertcontroller.addAction(UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler: {action in self.performSegue(withIdentifier: "goToSecond", sender: self)} ))
        present(alertcontroller, animated: true, completion: nil)
    }
    
    func showError(){
        let alertcontroller = UIAlertController(title: "Sorry", message:"Unable to login with provided credentials", preferredStyle: UIAlertControllerStyle.alert)
        alertcontroller.addAction(UIAlertAction(title:"Ok", style:UIAlertActionStyle.default, handler: nil ))
        present(alertcontroller, animated: true, completion: nil)
        
    }
    
    @IBAction func submit_Action(_ sender: Any) {
        username_input.resignFirstResponder()
        password_input.resignFirstResponder()
        let email = username_input.text ?? ""
        let password = password_input.text ?? ""
        guard email == "", password == "" else {
            let check_mail = isValidEmail(testStr: email)
            let check_pwd = isValidPassword(testStr: password)
            if check_mail && check_pwd == true{
                for items in username_array{
                    if items == email{
                        UserDefaults.standard.set(email, forKey: "mail")
                        t_mail = UserDefaults.standard.string(forKey: "mail")!
                        print(t_mail)
                        for (id, details) in userdetails_dict{
                            if id == t_mail{
                                UserDefaults.standard.set(details[0], forKey:"name")
                                UserDefaults.standard.set(details[2], forKey:"phone")
                                UserDefaults.standard.set(details[3], forKey:"image")
                            }
                        }
                        showAlert(name: email)
                        break
                    }
                }
            }
            showError()
        return
        }
        

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

