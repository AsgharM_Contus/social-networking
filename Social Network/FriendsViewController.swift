//
//  FriendsViewController.swift
//  Social Network
//
//  Created by user on 25/01/18.
//  Copyright © 2018 user. All rights reserved.
//

import UIKit

class FriendsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return friends_name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = table.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FriendsTableViewCell
        cell.f_name.text = friends_name[indexPath.row]
        cell.f_place.text = friends_place[indexPath.row]
        cell.f_email.text = friends_occupation[indexPath.row]
        cell.f_image.image = UIImage(named: friends_image[indexPath.row])
        //cell.f_image.image = UIImage(named: image)
        return cell
    }
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
            myGlobalVariables.iRow = indexPath.row
            
            self.performSegue(withIdentifier: "DetailView", sender: self)
        return
            }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "DetailView"){
            let vc = segue.destination as! DetailedViewController
            vc.p_name = "Your name"
            vc.p_mail = "Your mail"
        }
    }

//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//
//        if segue.identifier == "DetailView" {
//            var vc = segue.destination as! DetailedViewController
//           print(vc)
//        }
//    }

    @IBOutlet var table: UITableView!
//    var friends_array = ["jagen@contus.in" : ["Jagen K", "Bezant Nagar"],
//                         "pasupathy@contus.in" : ["Pasupathy", "Tambaram"]]
    var friends_name = ["Pasupathy","Santhosh","Kalandar"]
    var friends_place = [ "Tambaram", "Porur","Mount"]
    var friends_occupation = [ "Farmer","Engineer", "Doctor",]
    var friends_image = [ "pasupathy", "santhosh", "kalandar",]
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 256
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
